package com.latihan.microservice.productservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.latihan.microservice.productservice.dto.ProductRequest;
import com.latihan.microservice.productservice.dto.ProductResponse;
import com.latihan.microservice.productservice.dto.ResponseMessage;
import com.latihan.microservice.productservice.service.ProductService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/products")
@AllArgsConstructor
public class ProductController {

    // @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<ResponseMessage<List<ProductResponse>>> getAll()
    {
        ResponseMessage response = new ResponseMessage<>();
        response.setData(productService.getAll());
        response.setMessage("List data product.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseMessage<ProductResponse>> getById(@PathVariable Long id){
        ResponseMessage response = new ResponseMessage<>();
        response.setData(productService.getById(id));
        response.setMessage("Detail data product.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ResponseMessage<ProductResponse>> create(@RequestBody ProductRequest productRequest){
        ResponseMessage response = new ResponseMessage<>();
        response.setData(productService.create(productRequest));
        response.setMessage("Success create product.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseMessage<ProductResponse>> update(@PathVariable Long id, @RequestBody ProductRequest productRequest){
        ResponseMessage response = new ResponseMessage<>();
        response.setData(productService.update(id,productRequest));
        response.setMessage("Success update product.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseMessage<ProductResponse>> delete(@PathVariable Long id){
        ResponseMessage response = new ResponseMessage<>();
        response.setData(productService.delete(id));
        response.setMessage("Success delete product.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/checkAvailable/{id}/{quantity}")
    public ResponseEntity<Void> checkAvailable(@PathVariable Long id, @PathVariable int quantity){
        productService.checkAvailable(id,quantity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/reduceQuantity/{id}/{quantity}")
    public ResponseEntity<Void> reduceQuantity(@PathVariable Long id, @PathVariable int quantity){
        productService.reduceQuantity(id,quantity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
