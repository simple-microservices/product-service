package com.latihan.microservice.productservice.service.impl;

import com.latihan.microservice.productservice.dto.ProductRequest;
import com.latihan.microservice.productservice.dto.ProductResponse;
import com.latihan.microservice.productservice.entity.Product;
import com.latihan.microservice.productservice.exception.CustomException;
import com.latihan.microservice.productservice.repository.ProductRepository;
import com.latihan.microservice.productservice.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    public List<ProductResponse> getAll() {
        return productRepository.findAll().stream().map(product -> {
            return mapperProductToProductResponse(product);
        }).collect(Collectors.toList());
    }

    public ProductResponse getById(Long id) {
        Product product = productRepository.findById(id).orElseThrow(
                () -> new CustomException(
                        "Product with id " + id + " Not Found",
                        "PRODUCT_NOT_FOUND",
                        404
                ));
        return mapperProductToProductResponse(product);
    }

    public ProductResponse create(ProductRequest productRequest) {
        Product product = mapperProductRequestToProduct(productRequest);
        productRepository.saveAndFlush(product);
        return mapperProductToProductResponse(product);
    }

    public ProductResponse update(Long id, ProductRequest productRequest) {
        getById(id);
        Product product = mapperProductRequestToProduct(productRequest);
        product.setId(id);
        productRepository.saveAndFlush(product);
        return mapperProductToProductResponse(product);
    }

    public ProductResponse delete(Long id) {
        ProductResponse product = getById(id);
        productRepository.deleteById(id);
        return product;
    }

    @Override
    public void checkAvailable(Long productId, int quantity) {
        ProductResponse product = getById(productId);
        if (product.getQuantity() < quantity){
            throw new CustomException(
                    "Product does not have sufficient quantity",
                    "PRODUCT_INSUFFICIENT_QUANTITY",
                    400
            );
        }
    }

    @Override
    public void reduceQuantity(Long id, int quantity) {
        ProductResponse productResponse = getById(id);
        Product product = new Product();
        BeanUtils.copyProperties(productResponse,product);

        product.setQuantity(productResponse.getQuantity()-quantity);
        productRepository.save(product);
    }


    private ProductResponse mapperProductToProductResponse(Product product) {
        ProductResponse productResponse = new ProductResponse();
        BeanUtils.copyProperties(product, productResponse);
        return productResponse;
    }

    private Product mapperProductRequestToProduct(ProductRequest productRequest) {
        Product product = new Product();
        BeanUtils.copyProperties(productRequest, product);
        return product;
    }

}
