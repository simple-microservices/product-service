package com.latihan.microservice.productservice.exception;

import com.latihan.microservice.productservice.dto.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestResponseException {

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<ErrorMessage> customException(CustomException customException){
        return new ResponseEntity<>(
                new ErrorMessage().builder()
                        .message(customException.getMessage())
                        .error(customException.getError())
                        .build(),
                HttpStatusCode.valueOf(customException.getStatus())
        );
    }

}
