package com.latihan.microservice.productservice.service;

import com.latihan.microservice.productservice.dto.ProductRequest;
import com.latihan.microservice.productservice.dto.ProductResponse;
import com.latihan.microservice.productservice.entity.Product;

import java.util.List;

public interface ProductService {

    public List<ProductResponse> getAll();
    public ProductResponse getById(Long id);
    public ProductResponse create(ProductRequest productRequest);
    public ProductResponse update(Long id, ProductRequest productRequest);
    public ProductResponse delete(Long id);
    public void checkAvailable(Long productId,int quantity);

    public void reduceQuantity(Long id, int quantity);

}
