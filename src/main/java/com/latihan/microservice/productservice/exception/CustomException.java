package com.latihan.microservice.productservice.exception;

import lombok.Data;

@Data
public class CustomException extends RuntimeException{

    private String error; // PRODUCT_NOT_FOUND
    private int status; // 404

    public CustomException(String message, String error, int status) {
        super(message);
        this.error = error;
        this.status = status;
    }
}
