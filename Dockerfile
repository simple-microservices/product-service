FROM openjdk:17

ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} productservice.jar

ENTRYPOINT ["java","-jar","/productservice.jar"]

EXPOSE 8085

# 1. mvn clean install => Buat JAR file didalm folder target
# 2. docker build -t [namaId DockerHub]/[nama aplikasi]:[tag image (versi aplikasi)] .
# exm. docker build -t syafri/serviceregistry:0.0.1 .
# 3. docker images check docker images